//
// Created by thejackimonster on 14.04.20.
//

#ifndef CADET_GTK_GTK_H
#define CADET_GTK_GTK_H

#include <gtk/gtk.h>

void CGTK_activate(GtkApplication* application, gpointer user_data);

#endif //CADET_GTK_GTK_H
