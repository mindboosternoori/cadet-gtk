//
// Created by thejackimonster on 14.04.20.
//

#ifndef CADET_GTK_GNUNET_H
#define CADET_GTK_GNUNET_H

#include <gnunet/platform.h>
#include <gnunet/gnunet_util_lib.h>

void CGTK_run(void* cls, char*const* args, const char* cfgfile, const struct GNUNET_CONFIGURATION_Handle* cfg);

#endif //CADET_GTK_GNUNET_H
