
find_package(PkgConfig QUIET)

if (PKG_CONFIG_FOUND)
	pkg_check_modules(GTK3 REQUIRED gtk+-3.0)
	
	set(GTK_INCLUDE_DIR ${GTK3_INCLUDE_DIRS})
	set(GTK_LIBRARIES ${GTK3_LIBRARIES})
	
	list(APPEND CGTK_LINK_LIBRARIES ${GTK3_LIBRARY_DIRS})
	list(APPEND CGTK_DEFINITIONS ${GTK3_CFLAGS_OTHER})
else()
	find_package(GTK REQUIRED)
endif()

list(APPEND CGTK_INCLUDE_DIRECTORIES ${GTK_INCLUDE_DIR})
list(APPEND CGTK_LIBRARIES ${GTK_LIBRARIES})
